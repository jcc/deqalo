#!/usr/bin/python3
"""
Web front-end for deqalo.
"""

# Settings
DEBUG = True

# Imports
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from waitress import serve

# Instantiate flask app
app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/blank')
def blank_page():
    return render_template('blank.html')

if __name__ == '__main__':
    serve(app, host='127.0.0.1', port=8080)

