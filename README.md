## Deqalo

### About

"Deqalo" is a portmanteau of "Debian QA Logger". It's not specifically limited to that,
but it's the itch that it is originally intended to scratch.

Deqalo allows you to define products (For example: ISO images) and tests.
You can then assign tests to a product and let users or scripts log their results.

A release manager can then sign off on products when they are ready for release.

### Dependencies:

Debian packages required:

 - python3 - interactive high-level object-oriented language
 - python3-flask - micro web framework based on Werkzeug and Jinja2
 - python3-flaskext.wtf - Simple integration of Flask and WTForms (Python 3)
 - python3-waitress - production-quality pure-Python WSGI server
 - fonts-font-awesome - iconic font designed for use with Twitter Bootstrap

### Todo

 - Embedded CSS files will be dealt with, don't worry about them. 
